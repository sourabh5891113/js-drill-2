// 5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

let data = require("../data");
let getSalariesBasedOnCountry = require("../problems/problem5");

getSalariesBasedOnCountry(data); //we will get an object with key as country and sum of salaries as value.
