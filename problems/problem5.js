let convertSalaryIntoNumbers = require("./problem2");

function getSalariesBasedOnCountry(data) {
  if (Array.isArray(data)) {
    convertSalaryIntoNumbers(data);
    let salariesBasedOnCountry = {};

    for (let i = 0; i < data.length; i++) {
      let location = data[i].location;
      let salary = data[i].salary;

      if (salariesBasedOnCountry[location]) {
        salariesBasedOnCountry[location] = salariesBasedOnCountry[location] + salary;
        salariesBasedOnCountry[location] = Math.floor(salariesBasedOnCountry[location] * 100) / 100;
      } else {
        salariesBasedOnCountry[location] = salary;
      }
    }

    // console.log(salariesBasedOnCountry);
    return salariesBasedOnCountry;
  } else {
    console.log("First agrument should be an array");
    return {};
  }
}

module.exports = getSalariesBasedOnCountry;
