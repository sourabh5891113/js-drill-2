function convertSalaryIntoNumbers(data) {
  if (Array.isArray(data)) {
    let newData = [];
    for (let key of data) {
      let element = key;
      element.salary = Number(element.salary.substring(1));
      newData.push(element);
    }
    return newData;
  } else {
    console.log("First agrument should be an array");
    return [];
  }
}

module.exports = convertSalaryIntoNumbers;
