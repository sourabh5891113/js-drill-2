let convertSalaryIntoNumbers = require("./problem2");

function addCorrectedSalaryToData(data) {
  if (Array.isArray(data)) {
    let convertedData = convertSalaryIntoNumbers(data);
    for (let key of convertedData) {
      let salary = key.salary;
      key.corrected_salary = salary * 10000;
    }
    return convertedData;
  } else {
    console.log("First agrument should be an array");
    return [];
  }
}

module.exports = addCorrectedSalaryToData;
