let getSalariesBasedOnCountry = require("./problem5");

function getAverageSalaryBasedOnCountry(data) {
  if (Array.isArray(data)) {
    let salariesWRTcountry = getSalariesBasedOnCountry(data);
    let countriesCount = {};
    for (let i = 0; i < data.length; i++) {
      let location = data[i].location;
      if (countriesCount[location]) {
        countriesCount[location] = countriesCount[location] + 1;
      } else {
        countriesCount[location] = 1;
      }
    }

    for (let key in salariesWRTcountry) {
      salariesWRTcountry[key] = salariesWRTcountry[key] / countriesCount[key];
      salariesWRTcountry[key] = Math.floor(salariesWRTcountry[key] * 100) / 100;
    }
    console.log(salariesWRTcountry);
    return salariesWRTcountry;
  } else {
    console.log("First agrument should be an array");
    return {};
  }
}

module.exports = getAverageSalaryBasedOnCountry;
