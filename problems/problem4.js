let convertSalaryIntoNumbers = require("./problem2");

function getSalariesSum(data) {
  if (Array.isArray(data)) {
    let convertedSalaryData = convertSalaryIntoNumbers(data);
    let salariesSum = 0;
    for (let key of convertedSalaryData) {
      salariesSum += key.salary;
    }
    console.log(salariesSum);
  } else {
    console.log("First agrument should be an array");
    return [];
  }
}

module.exports = getSalariesSum;
