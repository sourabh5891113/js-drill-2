function findAllWebDevelopers(data) {
  if (Array.isArray(data)) {
    for (let key of data) {
      if (key.job.includes("Web Developer")) {
        console.log(key);
      }
    }
  } else {
    console.log("First agrument should be an array");
  }
}

module.exports = findAllWebDevelopers;
